#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# DST-RailJet-LongSeamlessLoop by Deceased Superior Technician (http://www.nosoapradio.us)
# Art by Kenney.nl, Ville Seppanen (villeseppanen.com), sigon

import pygame
import sys
import os.path
import random
from settings import *
from sprites import *

print('''
    KESSEL RUN Copyright (C) 2018 SIGON
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.
''')

#  new
class Game:
    def __init__(self):
        # initialize game window, etc
        pygame.mixer.pre_init(22050, -16, 2, 1024)
        pygame.init()
        pygame.mixer.init()
        self.assets_dir = os.path.join(os.path.split(os.path.abspath(__file__))[0], 'assets')
        self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
        pygame.display.set_caption(TITLE)
        icon = pygame.image.load(os.path.join(self.assets_dir, GAME_ICON)).convert_alpha()
        pygame.display.set_icon(icon)
        # Load game graphics
        self.background = pygame.image.load(os.path.join(self.assets_dir, GAME_BACKGROUND)).convert_alpha()
        self.background_rect = self.background.get_rect()
        self.clock = pygame.time.Clock()
        self.running = True

    def new(self):
        self.load_data()
        # Initialize Game Groups
        self.all_sprites = pygame.sprite.Group()

        self.ice_chucks = pygame.sprite.Group()
        self.lasers = pygame.sprite.Group()
        self.ties = pygame.sprite.Group()
        self.tie_lasers = pygame.sprite.Group()
        self.explosions = pygame.sprite.Group()

        self.player = Player(self)
        self.all_sprites.add(self.player)
        for i in range(CHUCKS_MAX): # 8 ice chucks at the same time in the screen
            Ice_Chuck(self)
        for i in range(TIES_MAX):
            Tie(self)

        self.run()

    def load_image(self, file):
        file = os.path.join(game.assets_dir, file)
        try:
            surface = pygame.image.load(file)
        except pygame.error:
            raise SystemExit('Could not load image "%s" %s'%(file, pygame.get_error()))
        # Convert returns us a new Surface of the image,
        # but now converted to the same pixel format as our display
        return surface.convert_alpha()

    def load_data(self):
        # load game explosions
        self.explosion_anim = {}
        self.explosion_anim['large'] = [] # large explosion for ice chucks
        self.explosion_anim['small'] = [] # small explosion for falcon damage
        self.explosion_anim['player'] = [] # falcon total explosion
        self.explosion_anim['tie'] = [] # tie explosion

        for i in range(9):
            file_name1 = os.path.join(self.assets_dir,'regularExplosion0{}.png'.format(i)) # img has 9 frames so we iterate 9 times
            img = pygame.image.load(file_name1).convert_alpha()
            img_lg = pygame.transform.scale(img, (75, 75))
            self.explosion_anim['large'].append(img_lg)
            img_sm = pygame.transform.scale(img, (32, 32))
            self.explosion_anim['small'].append(img_sm)

            file_name2 = os.path.join(self.assets_dir,'sonicExplosion0{}.png'.format(i))
            img2 = pygame.image.load(file_name2).convert_alpha()
            self.explosion_anim['player'].append(img2)

        # tie explosion
        for i in range(6):
            file_name3 = os.path.join(self.assets_dir,'red_explosion0{}.png'.format(i)) # img has 6 frames so we iterate 9 times
            img3 = pygame.image.load(file_name3).convert_alpha()
            self.explosion_anim['tie'].append(img)

        # Load game sounds:
        game_sounds_files = [LASER_SOUND, EXPLOSION_1_SOUND, EXPLOSION_2_SOUND, EXPLOSION_3_SOUND, HIT_SOUND, PLAYER_DIE_SOUND]
        self.sounds = []
        for file in game_sounds_files:
            sound_file = os.path.join(self.assets_dir, 'sounds', file)
            sound = pygame.mixer.Sound(sound_file)
            sound.set_volume(0.1)
            self.sounds.append(sound)
        music_file = os.path.join(self.assets_dir, 'sounds', GAME_MUSIC)
        self.music = pygame.mixer.music.load(music_file)
        pygame.mixer.music.set_volume(0.4)
        pygame.mixer.music.play(loops=-1)

    def run(self):
        # Game Loop
        self.playing = True
        while self.playing:
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()

    def draw_shield_bar(self, surf, x, y, pct):
        if pct < 0:
            pct = 0
        BAR_LENGTH = 100
        BAR_HEIGHT = 10
        fill = (pct / 100) * BAR_LENGTH
        outline_rect = pygame.Rect(x, y, BAR_LENGTH, BAR_HEIGHT)
        fill_rect = pygame.Rect(x, y, fill, BAR_HEIGHT)
        pygame.draw.rect(surf, GREEN, fill_rect)
        pygame.draw.rect(surf, WHITE, outline_rect, 2)

    def draw_thermal_display(self, surf, x, y, value):
        OUT_BAR_LENGTH = 100
        BAR_WIDTH = 10
        BAR_HEIGHT = 20
        color = RED
        outline_rect = pygame.Rect(x, y, OUT_BAR_LENGTH, BAR_HEIGHT)
        for i in range(0, 10):
            if value <= (i * 10) + 10:
                color = GREEN
            pos_x = x + 10 * i
            bar = pygame.Rect(pos_x, y, BAR_WIDTH, BAR_HEIGHT )
            pygame.draw.rect(surf, WHITE, bar, 3)
            pygame.draw.rect(surf, color, bar)
        pygame.draw.rect(surf, WHITE, outline_rect, 2)

    def draw_text(self, surf, text, size, x, y):
        font = pygame.font.Font(FONT_NAME, size)
        text_surface = font.render(text, True, WHITE)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def update(self):
        # Game Loop - Update
        self.all_sprites.update()

        # check to see if a laser bean hit an ice chuck
        hits = pygame.sprite.groupcollide(self.ice_chucks, self.lasers, True, True)
        for hit in hits:
            self.sounds[random.randrange(1,3)].play()
            expl = Explosion(self, hit.rect.center, 'large')
            # all_sprites.add(expl)
            Ice_Chuck(self)

        # check if an ice chuck hit the falcon (circle collision)
        hits = pygame.sprite.spritecollide(self.player, self.ice_chucks, True, pygame.sprite.collide_circle)
        for hit in hits:
            Ice_Chuck(game)
            self.sounds[4].play()
            self.player.health -= hit.radius/2
            Explosion(self, hit.rect.center, 'small')
            # all_sprites.add(expl)

        if self.player.health <= 0:
            self.sounds[5].play()
            Explosion(self, self.player.rect.center, 'player')
            # all_sprites.add(death_explosion)
            self.player.hide()
            self.player.lives -= 1
            self.player.health = 100

        # check to see if a tie's laser hit the falcon
        tie_hits = pygame.sprite.spritecollide(self.player, self.tie_lasers, True, pygame.sprite.collide_circle)
        for hit in tie_hits:
            self.sounds[4].play()
            self.player.health -= FALCON_DMG_TIE_SHOOT
            Explosion(self, hit.rect.center, 'small')
            # all_sprites.add(expl)

        # check to see if the falcon's laser hits a tie
        tie_hits = pygame.sprite.groupcollide(self.ties, self.lasers, True, True)
        for hit in tie_hits:
            self.sounds[4].play()
            Explosion(self, hit.rect.center, 'tie')
            # all_sprites.add(expl)
            Tie(self)

        # check if the falcon an a tie collide
        tie_falcon_hits = pygame.sprite.spritecollide(self.player, self.ties, True, pygame.sprite.collide_circle)
        for hit in tie_falcon_hits:
            self.sounds[4].play()
            self.player.health -= FALCON_DMG_TIE
            Explosion(self, hit.rect.center, 'small')
            # all_sprites.add(expl)
            Tie(self)

        # if the player died and the explosion has finished playing
        if self.player.lives == 0 and not death_explosion.alive():
            self.running = False

        # check if coaxium temperature has reach the maximun
        if self.player.thermal_display >= 100:
            self.running = False

        #

    def events(self):
        # Game Loop - events
        for event in pygame.event.get():
            # check for closing window
            if event.type == pygame.QUIT:
                if self.playing:
                    self.playing = False
                self.running = False

    def draw(self):
        # Game Loop - draw
        self.screen.fill(BACKGROUND_COLOR)
        self.screen.blit(self.background, self.background_rect)

        self.all_sprites.draw(self.screen)

        # info parsecs count
        msg = 'PARSECS: ' + str(self.player.parsecs)
        self.draw_text(self.screen, msg, 18, WIDTH / 2, 10)

        # shield status (falcon health)
        falcon_health = int(self.player.health)
        self.draw_shield_bar(self.screen, 5, 35, falcon_health)

        # thermal display coaxium
        self.draw_thermal_display(self.screen, 690, 5, self.player.thermal_display)

        # draw player lives
        Lives_Display(self.screen, 5, 5, self.player.lives-1, self.player.mini_img)

        # *after* drawing everything, flip the display
        pygame.display.flip()

    def show_start_screen(self):
        # game splash/start screen
        pass

    def show_go_screen(self):
        # game over/continue
        pass

game = Game()
game.show_start_screen()
while game.running:
    game.new()
    game.show_go_screen()

pygame.quit()
