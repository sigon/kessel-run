# KESSEL RUN

![License: GPL v3+](https://img.shields.io/badge/license-GPL%20v3%2B-blue.svg)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/Django.svg)
![release - Version](https://img.shields.io/badge/version-v1.0.0-green.svg)

A pygame

The **Kessel Run** is a route used by smugglers to move drugs without the empire to notice, this run is normally 20 parsecs long.

The ship's cargo is not drugs but **coaxium**. Unrefined coaxium needs to be kept cool in canisters and must reach a refinery before the internal temperature gets too warm and explodes.

So, our hero needs to **make the Kessel Run in less than 12 parsecs** because is the only way to make it before the coaxium explode.

Parsec is a unit of distance, not time, so we are not referring 'directly' to the ship speed. Our pilot is taking a dangerous shortcut into the [Maelstrom](http://starwars.wikia.com/wiki/Akkadese_Maelstrom) (interstellar gas and ice chucks that surrounded the Kessel Planet) so he can make the run in time just before the coaxium temperature goes so high.

Are you able to complete the Kessel Run before the coaxium turn red and explodes?

## Events:

[x] TIE fighters

[ ] shortcut into the maelstrom

[x] ice chucks

[ ] summa-verminoth

[ ] The maw (gravitational well)

[ ] launch the escape pod

[ ] coaxium boost to go outside the maelstrom

[ ] lightspeed

[ ] arrive at Savareen

## Efects

[ ] falcon blue engine

[ ] backwards laser fire

[x] parsec count

[x] live count

[x] coaxium temperature indicator


![kessel run gif](kesselRun.gif)

## LINKS:

https://www.pygame.org/docs/

https://nerdparadise.com/programming/pygame

## This game was creating following this tutorial by Kids Can Code:

https://github.com/kidscancode/pygame_tutorials/tree/master/shmup

### Open game Art

https://opengameart.org/

## License

Copyright (C) 2018 by sigon

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
