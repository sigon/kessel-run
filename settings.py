import pygame


# game constants

WIDTH = 800 # screen
HEIGHT = 600
BACKGROUND_COLOR = (105,105,105)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
FPS = 60 # frames per second to update the screen
TITLE = "Kessel Run"
GAME_ICON = "falcon-icon.png"
GAME_BACKGROUND = "background.png"
FONT_NAME = pygame.font.match_font('arial')

# GAME sounds

LASER_SOUND = "Shoot.wav"
EXPLOSION_1_SOUND = "expl3.wav"
EXPLOSION_2_SOUND = "expl6.wav"
EXPLOSION_3_SOUND = "Explosion.wav"
HIT_SOUND = "Hit.wav"
PLAYER_DIE_SOUND = 'rumble1.ogg'
GAME_MUSIC = "DST-RailJet-LongSeamlessLoop.ogg"


# Game actions
FIRE_KEY = pygame.K_SPACE
RIGHT_KEY = pygame.K_RIGHT
LEFT_KEY = pygame.K_LEFT
UP_KEY = pygame.K_UP
DOWN_KEY = pygame.K_DOWN
ADD_COAXIUM = pygame.K_w # increase the Falcon velocity

# game rules / constraints:
COAXIUM_TIME = 3 * 60 * 1000 # 3 minutes
KESSEL_RUN_PARSECS = 12

CHUCKS_MAX = 6 # maximun number of ice chucks on screen

TIE_SPEED = 6 # self.speedx
TIE_LIMIT = 3 # parsecs, new ties wont appear bellow that point
TIE_LASER_LIMIT = WIDTH * 0.6 # max distance of the fire bean
TIE_LASER_SPEED = 10 # self.speedy
TIE_SHOOT_LIMIT = WIDTH * 0.6 # ties stop shooting when certain point is passed
TIES_MAX = 3 # maximun number of ties

FALCON_SPEED = 4 # parsecs/minute (to make the kessel run in 3 minutes)
FALCON_LIVES = 7 # number of lives at start
FALCON_INIT_HEALTH = 100
FALCON_LASER_LIMIT = WIDTH * 0.6 # max distance of the fire bean
FALCON_LASER_SPEED = 10 # self.speedy
FALCON_DMG_TIE = 20 # damage in health caused by a tie crash on the falcon
FALCON_DMG_TIE_SHOOT = 5 # damage in health caused by a tie shoot
