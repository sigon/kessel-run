# Sprite classes for platform game
import pygame
import random

from settings import *


class Player(pygame.sprite.Sprite):
    def __init__(self , game):
        self.groups = game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game

        self.height = 100
        self.width = 73
        self.y = HEIGHT/2
        self.x = self.width
        self.speed = 5
        self.speedx = 0
        self.speedy = 0
        self.health = FALCON_INIT_HEALTH # falcon initial health or shield protecction

        # load falcon image
        falcon_orig_image = game.load_image('millenium-falcon.png')
        falcon_lando_image = game.load_image('millenium-falcon-lando.png')
        falcon_small = pygame.transform.scale(falcon_lando_image, (self.width, self.height))
        falcon_img = pygame.transform.rotate(falcon_small, 270)
        falcon_mini_img = pygame.transform.scale(falcon_orig_image, (19, 25))
        self.mini_img = falcon_mini_img

        # sprite requires a .image and .rect atributtes
        self.image = falcon_img
        self.rect = self.image.get_rect()
        self.radius = 35

        self.rect.center = (self.x, self.y) # initial position
        self.shoot_delay = 250 # ms
        self.last_shot = pygame.time.get_ticks()

        # falcon parsecs count
        self.parsecs = 0
        self.aceleration = 0

        self.lives = FALCON_LIVES
        self.hidden = False
        self.hide_timer = pygame.time.get_ticks()

    def update(self):

        if self.lives == 0:
            self.kill()
        # unhide if hidden
        if self.hidden and pygame.time.get_ticks() - self.hide_timer > 1000:
            self.hidden = False
            self.rect.center = (10, HEIGHT/2)

        self.speedx = 0
        self.speedy = 0

        # capture cursor movement
        pressed = pygame.key.get_pressed()
        if not self.hidden:
            if pressed[UP_KEY]: self.speedy = -self.speed
            if pressed[DOWN_KEY]: self.speedy = self.speed
            if pressed[LEFT_KEY]: self.speedx = -self.speed
            if pressed[RIGHT_KEY]: self.speedx = self.speed
            if pressed[FIRE_KEY]: self.shoot()

        # falcon velocity
        now = pygame.time.get_ticks()
        if pressed[ADD_COAXIUM]:
            self.aceleration += 0.0010
            self.speedx = 2

        total_parsecs = now * FALCON_SPEED/60000 + self.aceleration
        self.parsecs = round(total_parsecs, 2)

        # constrains
        if self.rect.top < 10:
            self.rect.top = 10
        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.bottom > HEIGHT:
            self.rect.bottom = HEIGHT

        # update falcon position
        self.rect.x += self.speedx
        self.rect.y += self.speedy

        # coaxium temperature
        self.thermal_display = int(pygame.time.get_ticks())*100/ COAXIUM_TIME

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            self.game.sounds[0].play()
            Laser(self.game, self.rect.centerx, self.rect.centery)
            # all_sprites.add(laser)
            # lasers.add(laser)

    def hide(self):
        # hide the player temporarily
        self.hidden = True
        self.hide_timer = pygame.time.get_ticks()
        self.rect.center = (10, HEIGHT/2)


class Tie_Laser(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites, game.tie_lasers
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game

        laser_img = pygame.transform.rotate(self.game.load_image('laserGreen13.png'), 270)
        self.image = laser_img
        self.rect = self.image.get_rect()
        self.rect.center = (x + 30, y)
        self.speedy = TIE_LASER_SPEED
        self.fire_origin = self.rect.centerx

    def update(self):
        self.rect.x += self.speedy
        fire_position = self.rect.x
        fire_distance = fire_position - self.fire_origin

        # limit the distance of the fire bean
        if fire_distance > TIE_LASER_LIMIT:
            self.kill()


class Ice_Chuck(pygame.sprite.Sprite):

    def __init__(self, game):
        self.groups = game.all_sprites, game.ice_chucks
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game

        # load ice chuck images and put it randomly on each sprite
        ice_images = [game.load_image('meteorGrey_big2.png'),game.load_image('meteorGrey_med1.png')]
        ice_image = ice_images[random.randrange(2)]
        self.image_orig = ice_image
        self.image = self.image_orig.copy()
        self.rect = self.image.get_rect()
        self.radius = int(self.rect.width * .8/2)
        self.rect.x = random.randrange(WIDTH, WIDTH + 100)
        self.rect.y = random.randrange(-20, HEIGHT)

        self.speedy = random.randrange(-3, 3)
        self.speedx = random.randrange(-6, 4)

        # ice chuck rotation
        self.rot = 0
        self.rot_speed = random.randrange(-8, 8)
        self.last_update = pygame.time.get_ticks()


    # ice rotation
    def rotate(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > 50:
            self.last_update = now
            self.rot = (self.rot + self.rot_speed) % 360
            new_image = pygame.transform.rotate(self.image_orig, self.rot)
            old_center = self.rect.center
            self.image = new_image
            self.rect = self.image.get_rect()
            self.rect.center = old_center

    # ice moving
    def update(self):
        self.rotate()
        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.top < -10 or self.rect.bottom > HEIGHT or self.rect.left < -20:
            self.rect.x = random.randrange(WIDTH, WIDTH+100)
            self.rect.y = random.randrange(-20, HEIGHT)
            self.speedy = random.randrange(-5, 3)
            self.speedx = random.randrange(-5, -1)
        if self.rect.x > WIDTH + 100: # todo: find a better solution
            self.rect.x = WIDTH

# falcon laser cannon laser
class Laser(pygame.sprite.Sprite):
    def __init__(self, game, x, y):
        self.groups = game.all_sprites, game.lasers
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game
        laser_img = pygame.transform.rotate(game.load_image('laserRed01.png'), 270)
        self.image = laser_img
        self.rect = self.image.get_rect()
        self.rect.center = (x,y)
        self.speedy = FALCON_LASER_SPEED
        self.fire_origin = self.rect.centerx

    def update(self):
        self.rect.x += self.speedy
        fire_position = self.rect.x
        fire_distance = fire_position - self.fire_origin

        if fire_distance > FALCON_LASER_LIMIT:
            self.kill()

class Tie(pygame.sprite.Sprite):

    def __init__(self, game):
        self.groups = game.all_sprites, game.ties
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game

        tie_img = game.load_image('tie.png')
        self.image = tie_img
        self.rect = self.image.get_rect()
        self.radius = int(self.rect.width * .8/2)
        self.rect.x = random.randrange(-70, -10)
        self.rect.y = random.randrange(-20, HEIGHT-20)
        self.speedy = random.randrange(-3, 3)
        self.speedx = TIE_SPEED
        self.shoot_delay = 250 # ms
        self.last_shot = pygame.time.get_ticks()

    def update(self):
        self.rect.x += self.speedx
        self.rect.y += self.speedy
        if self.rect.top < -10 or self.rect.bottom > HEIGHT or self.rect.left > WIDTH + 50:
            self.rect.x = random.randrange(-70, -10)
            self.rect.y = random.randrange(-20, HEIGHT)
            self.speedy = random.randrange(-3, 3)
            self.speedx = TIE_SPEED
        if self.rect.centerx < TIE_SHOOT_LIMIT:
            self.shoot()
        if self.game.player.parsecs > TIE_LIMIT:
            self.kill()

    def shoot(self):
        now = pygame.time.get_ticks()
        if now - self.last_shot > self.shoot_delay:
            self.last_shot = now
            self.game.sounds[0].play()
            tie_laser1 = Tie_Laser(self.game, self.rect.centerx, self.rect.centery-3)
            tie_laser2 = Tie_Laser(self.game, self.rect.centerx, self.rect.centery+3)
            # all_sprites.add(tie_laser1)
            # all_sprites.add(tie_laser2)
            # tie_lasers.add(tie_laser1)
            # tie_lasers.add(tie_laser2)

# explosion when ice chuck are destroyed
class Explosion(pygame.sprite.Sprite):
    def __init__(self, game, center, size):

        self.groups = game.all_sprites, game.explosions
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.game = game

        self.size = size
        self.image = self.game.explosion_anim[self.size][0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0
        self.last_update = pygame.time.get_ticks()
        self.frame_rate = 75 # veloc of the explosion

    def update(self):
        now = pygame.time.get_ticks()
        if now - self.last_update > self.frame_rate:
            self.last_update = now
            if self.frame == len(self.game.explosion_anim[self.size]):
                self.kill()
            else:
                center = self.rect.center
                self.image = self.game.explosion_anim[self.size][self.frame]
                self.rect = self.image.get_rect()
                self.rect.center = center
            self.frame += 1

class Lives_Display:
    def __init__(self, surf, x, y, lives, img):

        for i in range(lives):
            img_rect = img.get_rect()
            img_rect.x = x + 30 * i
            img_rect.y = y
            surf.blit(img, img_rect)
